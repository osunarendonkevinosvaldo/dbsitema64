package modelo;


import java.sql.SQLException;
import java.util.ArrayList;
import modelo.dbManejador;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author hp
 */
public class dbProductos extends dbManejador  implements Persistencia{
    
    public dbProductos(){
        super();
        
    }

    @Override
    public void insertar(Object object) throws Exception {
        Productos pro= new Productos();
        pro = (Productos) object ;
        
        String consulta = "";
        consulta = "insert into productos (codigo, nombre, precio, fecha, status)values (?,?,?,?,0)";
        
        
        
        
        if (this.conectar()){
            
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, pro.getCodigo());
            this.sqlConsulta.setString(2, pro.getNombre());
            this.sqlConsulta.setFloat(3, pro.getPrecio());
            this.sqlConsulta.setString(4, pro.getFecha());
            //this.sqlConsulta.setInt(5, pro.getStatus());
            
            
            this.sqlConsulta.executeUpdate();
            this.desconectar();
            
        }
        
        
    }

    @Override
    public void actualizar(Object object) throws Exception {
        
        Productos pro= new Productos();
        pro = (Productos) object ;
        
        String consulta = "";
        consulta = " update productos set nombre= ?, precio=?, fecha=?,status=? where codigo = ?";
        
        if (this.conectar()){
        this.sqlConsulta = this.conexion.prepareStatement(consulta);
        this.sqlConsulta.setString(1, pro.getNombre());
        this.sqlConsulta.setFloat(2, pro.getPrecio());
        this.sqlConsulta.setString(3, pro.getFecha());
        this.sqlConsulta.setInt(4, pro.getStatus());
        this.sqlConsulta.setString(5, pro.getCodigo());
        
        
            this.sqlConsulta.executeUpdate();
            this.desconectar();
            
        }
   }

    @Override
    public void habilitar(Object object) throws Exception {
        
        Productos pro = (Productos) object;
        String consulta = "UPDATE productos set status = 0 WHERE codigo = ?";
        
        
        
        if (this.conectar()) {
        this.sqlConsulta = this.conexion.prepareStatement(consulta);
        this.sqlConsulta.setString(1, pro.getCodigo());

        
            this.sqlConsulta.executeUpdate();
            this.desconectar();
        }

    }
        
        

    @Override
    public void deshabilitar(Object object) throws Exception {
        Productos pro = (Productos) object;
        String consulta = "UPDATE productos set status = 1 WHERE codigo = ?";

        

        if (this.conectar()) {
            
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, pro.getCodigo());
            this.sqlConsulta.executeUpdate();
            this.desconectar();
        }
    }

    @Override
    public boolean siExiste(String codigo) throws Exception {
        boolean existe = false;
        String consulta = "SELECT 1 FROM productos WHERE codigo = ?";

        if (this.conectar()) {
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, codigo);
            this.registros = this.sqlConsulta.executeQuery();
            if (this.registros.next()) {
                existe = true;
            }
            this.desconectar();
        }

        return existe;
    }

    @Override
    public ArrayList lista(Object objetc) throws SQLException {
        ArrayList listaProductos = new ArrayList<Productos>();
        String consulta = "select * from productos where status = 0 order by codigo";
        
        if (this.conectar()){
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            registros = this.sqlConsulta.executeQuery();
            
            while (registros.next()){
            Productos pro = new Productos();
                    pro.setCodigo(registros.getString("codigo"));
                    pro.setNombre(registros.getNString("nombre"));
                    pro.setPrecio(registros.getFloat("precio"));
                    pro.setFecha(registros.getString("fecha"));
                    pro.setIdProductos(registros.getInt("idProductos"));
                    pro.setStatus(registros.getInt("status")); 
                    
                    //agregarlo al arrayList
                    listaProductos.add(pro);
            
            
            }
            
        }
            this.desconectar();
            return listaProductos;
        
    }

    @Override
    public Object buscar(String codigo) throws Exception {
        
    
        Productos pro= new Productos();
        String consulta = "SELECT * FROM productos WHERE codigo = ? and status = 0";

        

            if (this.conectar()) {
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                this.sqlConsulta.setString(1, codigo);
                registros = this.sqlConsulta.executeQuery();
                
                if (registros.next()) {
                    pro.setCodigo(codigo);
                    pro.setNombre(registros.getNString("nombre"));
                    pro.setPrecio(registros.getFloat("precio"));
                    pro.setFecha(registros.getString("fecha"));
                    pro.setIdProductos(registros.getInt("idproductos"));
                    pro.setStatus(registros.getInt("status"));
                 
                }

                //this.desconectar();
               
            }
        
            return pro;
    }
   
    
    }
    
    
    
    

