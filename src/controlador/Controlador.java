/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import Vista.*;
import modelo.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.Date;
import java.sql.SQLException;
import javax.swing.JOptionPane;

// manejo de fechas
import java.text.SimpleDateFormat;
import java.text.ParseException;
import com.toedter.calendar.JDateChooser;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;
//import javax.swing.JOptionPane;
/**
 *
 * @author hp
 */
public class Controlador implements ActionListener {

    private JifProductos vista;
    private dbProductos db;
    private boolean EsActualizar;
    private int idProducto = 0;

    public Controlador(JifProductos vista, dbProductos db) {
        this.vista = vista;
        this.db = db;

        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnDeshabilitar.addActionListener(this);
        vista.btnBuscar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnHabilitar.addActionListener(this);

    }

    public void limpiar() {
        vista.txtCodigo.setText("");
        vista.txtNombre.setText("");
        vista.txtPrecio.setText("");
        vista.dftFecha.setDate(null);
    }

    public void cerrar() {
        int res = JOptionPane.showConfirmDialog(vista, "Desea cerrar el sistema ",
                "Productos", JOptionPane.YES_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (res == JOptionPane.YES_OPTION) {
            vista.dispose();
        }

    }

    public void habilitar() {
        vista.txtCodigo.setEnabled(true);
        vista.txtNombre.setEnabled(true);
        vista.txtPrecio.setEnabled(true);
        
        vista.btnBuscar.setEnabled(true);
        vista.btnDeshabilitar.setEnabled(true);
        vista.btnHabilitar.setEnabled(true);
        vista.btnGuardar.setEnabled(true);
        //vista.btnNuevo.setEnabled(true);
        
        
        vista.dftFecha.setEnabled(true);

    }

    public void deshabilitar() {
        vista.txtCodigo.setEnabled(false);
        vista.txtNombre.setEnabled(false);
        vista.txtPrecio.setEnabled(false);
        vista.dftFecha.setEnabled(false);
        vista.btnBuscar.setEnabled(false);
        vista.btnGuardar.setEnabled(false);
        vista.btnHabilitar.setEnabled(false);
        vista.btnDeshabilitar.setEnabled(false);

    }

    public boolean validar() {
        boolean exito = true;
        if (vista.txtCodigo.getText().equals("")
                || vista.txtNombre.getText().equals("")
                || vista.dftFecha.getDate() == null
                || vista.txtPrecio.getText().equals("")) {
            exito = false;
        }

        return exito;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() == vista.btnLimpiar) {
            this.limpiar();
        }
        if (ae.getSource() == vista.btnCancelar) {
            this.limpiar();
            this.deshabilitar();
        }
        if (ae.getSource() == vista.btnCerrar) {
            this.cerrar();
        }

        if (ae.getSource() == vista.btnNuevo) {
            this.habilitar();
            this.EsActualizar = false;
        }
        
        if (ae.getSource() == vista.btnGuardar) {
            // Validar los datos de entrada
            if (this.validar()) {
                Productos pro = new Productos();
                pro.setCodigo(vista.txtCodigo.getText());
                pro.setNombre(vista.txtNombre.getText());
                pro.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
                pro.setStatus(0);
                pro.setFecha(this.convertirAñoMesDia(vista.dftFecha.getDate()));

                try {
                    
                    if (this.EsActualizar == false) {
                        if (db.siExiste(pro.getCodigo())) {
                            JOptionPane.showMessageDialog(vista, "El código ya existe. Por favor, use un código diferente.");
                            vista.txtCodigo.requestFocusInWindow();
                        } else{
                            // Agregar nuevo producto
                            db.insertar(pro);
                            JOptionPane.showMessageDialog(vista, "Se agregó con éxito");
                            this.limpiar();
                            this.deshabilitar();
                            this.ActualizarTabla(db.lista(vista));
                        }
                            
                        } else {
                        // Actualizar producto existente
                        pro.setIdProductos(idProducto);
                        db.actualizar(pro);
                        JOptionPane.showMessageDialog(vista, "Se actualizó el producto con éxito");
                        this.ActualizarTabla(db.lista(vista));
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(vista, "Surgió un error: " + ex.getMessage());
                }
            } else {            
                JOptionPane.showMessageDialog(vista, "Faltaron datos por capturar");
            }
        }
        

        if (ae.getSource() == vista.btnBuscar) {
            Productos pro = new Productos();

            if (vista.txtCodigo.getText().equals("")) {
                JOptionPane.showMessageDialog(vista, "Falto capturar el codigo");

            } else {
                try {
                    pro = (Productos) db.buscar(vista.txtCodigo.getText());
                    if (pro.getIdProductos() != 0) {
                        idProducto = pro.getIdProductos();
                        vista.txtNombre.setText(pro.getNombre());
                        vista.txtPrecio.setText(String.valueOf(pro.getPrecio()));
                        convertirStrindDate(pro.getFecha());
                        this.EsActualizar = true;
                        vista.btnDeshabilitar.setEnabled(true);
                        vista.btnGuardar.setEnabled(true);
                    } else {
                        JOptionPane.showMessageDialog(vista, "no se encontro");
                    }

                } catch (Exception e) {
                    JOptionPane.showMessageDialog(vista, "Surgio un error " + e.getMessage());
                }
            }
        }

        if (ae.getSource() == vista.btnDeshabilitar) {

            if (vista.txtCodigo.getText().equals("")) {
                JOptionPane.showMessageDialog(vista, "Favor de capturar el codigo del producto a deshabilitar");
            } else {
                int opcion = 0;
                opcion = JOptionPane.showConfirmDialog(vista, "¿ Deseas deshabilitar el producto? ", "Producto",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if (opcion == JOptionPane.YES_OPTION) {
                    Productos pro = new Productos();
                    pro.setCodigo(vista.txtCodigo.getText());
                    pro.setIdProductos(idProducto);
                    
                    try {
                        db.deshabilitar(pro);
                        JOptionPane.showMessageDialog(vista, "El producto ha sido deshabilitado");
                        this.limpiar();
                        this.deshabilitar();
                        this.ActualizarTabla(db.lista(vista));
                        //Actualizar la tabla;

                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(vista, "Surgio un error" + e.getMessage());
                    }

                }
            }
        }
        if (ae.getSource() == vista.btnHabilitar) {
            if (vista.txtCodigo.getText().equals("")) {
                JOptionPane.showMessageDialog(vista, "Favor de capturar el codigo del producto que desea habilitar");
            } else {
                int opcion = 0;
                opcion = JOptionPane.showConfirmDialog(vista, "¿Deseas habilitar el producto? ", "Producto",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                if (opcion == JOptionPane.YES_OPTION) {
                    Productos pro = new Productos();
                    pro.setCodigo(vista.txtCodigo.getText());
                    pro.setIdProductos(idProducto);

                    try {
                        db.habilitar(pro);
                        JOptionPane.showMessageDialog(vista, "El producto ha sido habilitado");
                        this.limpiar();
                        this.deshabilitar();
                        this.ActualizarTabla(db.lista(vista));
                        //Actualizar la tabla;

                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(vista, "Surgio un error" + e.getMessage());
                    }
                }
            }
        }

    }

    public void iniciarVista() {
        vista.setTitle("Producto");
        vista.resize(900, 700);
        vista.setVisible(true);
        vista.setLocale(null);
        this.deshabilitar();
        
        try{
        this.ActualizarTabla(db.lista(vista));
        }catch (Exception e){
            JOptionPane.showMessageDialog(vista, "Surgio un error" + e.getMessage());
        }
    }

    public String convertirAñoMesDia(Date fecha) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(fecha);
    }

    public void convertirStrindDate(String fecha) {
        try {
            // convertir la cadena de texto a un objeto Date
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyy-MM-dd");
            Date date = dateFormat.parse(fecha);
            vista.dftFecha.setDate(date);

        } catch (ParseException e) {
            System.err.print(e.getMessage());
        }

    }
    
    public void ActualizarTabla(ArrayList<Productos> arr){
        String campos[] = {"idProductos","Codigo","Nombre","Precio","Fecha"} ;
        
        String[][] datos = new String[arr.size()][5];
        int region = 0;
        for(Productos registro : arr){
            datos[region][0]= String.valueOf(registro.getIdProductos());
            datos[region][1]= registro.getCodigo();
            datos[region][2]= registro.getNombre();
            datos[region][3]= String.valueOf(registro.getPrecio());
            datos[region][4]= registro.getFecha();
            
            region ++;
        }
        DefaultTableModel tb = new DefaultTableModel(datos,campos);
        vista.lista.setModel(tb);
        
    }

}
