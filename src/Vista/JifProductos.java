/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

/**
 *
 * @author hp
 */
public class JifProductos extends javax.swing.JInternalFrame {

    /**
     * Creates new form JifAlumnos
     */
    public JifProductos() {
        initComponents();
        resize(820, 600);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        txtPrecio = new javax.swing.JTextField();
        txtCodigo = new javax.swing.JTextField();
        dftFecha = new com.toedter.calendar.JDateChooser();
        btnCerrar = new javax.swing.JButton();
        btnBuscar = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        lista = new javax.swing.JTable();
        btnDeshabilitar = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnHabilitar = new javax.swing.JButton();

        setBackground(new java.awt.Color(0, 204, 204));
        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setTitle("Productos");
        getContentPane().setLayout(null);

        jLabel1.setBackground(new java.awt.Color(204, 204, 255));
        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("Nombre :");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(0, 90, 140, 60);

        jLabel2.setBackground(new java.awt.Color(204, 204, 255));
        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel2.setText("Precio :");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(10, 160, 140, 60);

        jLabel3.setBackground(new java.awt.Color(204, 204, 255));
        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setText("Codigo :");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(10, 20, 140, 60);

        jLabel4.setBackground(new java.awt.Color(204, 204, 255));
        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel4.setText("Fecha :");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(530, 0, 140, 60);

        txtNombre.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        getContentPane().add(txtNombre);
        txtNombre.setBounds(90, 100, 230, 40);

        txtPrecio.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        getContentPane().add(txtPrecio);
        txtPrecio.setBounds(90, 170, 230, 40);

        txtCodigo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        getContentPane().add(txtCodigo);
        txtCodigo.setBounds(90, 30, 230, 40);

        dftFecha.setDateFormatString("yyyy/MM/dd");
        getContentPane().add(dftFecha);
        dftFecha.setBounds(610, 20, 180, 26);

        btnCerrar.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnCerrar.setText("CERRAR");
        getContentPane().add(btnCerrar);
        btnCerrar.setBounds(580, 460, 180, 60);

        btnBuscar.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnBuscar.setText("<< BUSCAR");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });
        getContentPane().add(btnBuscar);
        btnBuscar.setBounds(330, 30, 160, 40);

        btnNuevo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnNuevo.setText("NUEVO");
        getContentPane().add(btnNuevo);
        btnNuevo.setBounds(590, 60, 180, 70);

        lista.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(lista);

        getContentPane().add(jScrollPane1);
        jScrollPane1.setBounds(10, 330, 780, 110);

        btnDeshabilitar.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnDeshabilitar.setText("DESHABILITAR");
        getContentPane().add(btnDeshabilitar);
        btnDeshabilitar.setBounds(590, 220, 180, 70);

        btnLimpiar.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnLimpiar.setText("LIMPIAR");
        getContentPane().add(btnLimpiar);
        btnLimpiar.setBounds(60, 460, 180, 60);

        btnCancelar.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnCancelar.setText("CANCELAR");
        getContentPane().add(btnCancelar);
        btnCancelar.setBounds(320, 460, 180, 60);

        btnGuardar.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnGuardar.setText("GUARDAR");
        getContentPane().add(btnGuardar);
        btnGuardar.setBounds(590, 140, 180, 70);

        btnHabilitar.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnHabilitar.setText("HABILITAR");
        btnHabilitar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHabilitarActionPerformed(evt);
            }
        });
        getContentPane().add(btnHabilitar);
        btnHabilitar.setBounds(390, 220, 190, 70);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnHabilitarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHabilitarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnHabilitarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnBuscar;
    public javax.swing.JButton btnCancelar;
    public javax.swing.JButton btnCerrar;
    public javax.swing.JButton btnDeshabilitar;
    public javax.swing.JButton btnGuardar;
    public javax.swing.JButton btnHabilitar;
    public javax.swing.JButton btnLimpiar;
    public javax.swing.JButton btnNuevo;
    public com.toedter.calendar.JDateChooser dftFecha;
    public javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JTable lista;
    public javax.swing.JTextField txtCodigo;
    public javax.swing.JTextField txtNombre;
    public javax.swing.JTextField txtPrecio;
    // End of variables declaration//GEN-END:variables
}
